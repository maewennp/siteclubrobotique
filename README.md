# Projet-site-asso

Création d'un site web pour l'association de robotique du CESI

Projet collaboratif (groupe de 3)

Langages HTML, CSS, JS, PHP - Thème "cyberpunk"

# Démarrage Docker 
Pour lancer le docker avec le paramétrage, les commandes sont à taper dans le terminal à la racine du dossier : 
```bash
docker-compose build #Attendre la fin du build
docker-compose up #Attendre la fin du lancement
```

Sur internet taper l'adresse : **127.0.0.1:8065**.
