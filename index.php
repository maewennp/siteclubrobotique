<!--Page Accueil-->

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <!--Pour que la responsivité s'active quand on inspecte (F12)-->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 
        <!-- lien pour le js -->
        <script src="header_footer_body/header_anim.js" defer="defer"></script>
        <script src="https://kit.fontawesome.com/d93ad3eb56.js" crossorigin="anonymous"></script>
        <!-- lien pour les fichiers de style (css) -->
        <link rel="stylesheet" href="header_footer_body/header_footer.css">
        <link rel="stylesheet" href="header_footer_body/body.css">
        <link rel="stylesheet" href="accueil/accueil.css">
        <!--pour la typo google font Raleway-->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;300;400&display=swap" rel="stylesheet"> 
    </head>
    <body>
        <?php include('./header_footer_body/header.php'); ?> <!-- appel du fichier header.php-->

        <main class="vertical">
            <!-- <section class="accueil_event">         bandeau pour les events 
                <img src="" alt="event_photo">
                <h2> Event + date </h2>
                <p> blablabla </p>
                <ul>
                    <li><a href="arctile_connecte"> titre arcticle</a></li>
                    <li><a href="arctile_connecte"> titre arcticle</a></li>
                    <li><a href="arctile_connecte"> titre arcticle</a></li>
                </ul>
                <img src="" alt="event_agrandi">
            </section> -->
        
            <section class="horizontal">  <!-- 1er bloc texte + photo -->
                                
                <div class="zone_text">
                    <h1> A propos </h1>
                    <p> 
                        Fondé en 2023, CESI Robotique est une association étudiante qui regroupent des élèves en formation au CESI de Brest ainsi que des intervenants donnant des cours dans ce même établissement. <br />

                        <br />Notre association s’adressent à toute personne, peu importe le statut ou le cursus suivi, s’intéressant au domaine de la robotique et de l’électronique. Rattachée à l’école, le club évolue en parallèle de celle-ci, profitant du matériel et des locaux mis à disposition par l’établissement en échange d’une participation aux divers évènements organisés par le CESI pour mettre en avant les opportunités que l’école peut offrir.<br /> 

                        <br />Le club propose également ses propres activités. De l’impression 3D, des ateliers d’électronique embarquée, de la conception de gadgets connectés, du prototypage sur carte ESP32 et Arduino, … Les seules limites : votre imagination et la disponibilité du filament d’impression !
                    </p>
                </div>

                <div class="pics1">
                    <img src="img/giphy.gif" alt="qui_sommes_nous">
                </div>
                
            </section>
        
            <section class="horizontal"> <!-- 2eme bloc, dans le même style que le 1er mais avec la photo et le texte inversés -->
                <div class="pics2">
                    <img src="img/giphy.gif" alt="que_fait_on">
                </div>
                
                <div class="zone_text">
                    <h1> Que fait-on ? </h1>
                    <p> Dans notre club de robotique, nous explorons un monde passionnant où la créativité et la technologie se rencontrent. <br /> 
                        <br />Notre principal domaine d'activité est l'impression 3D, une technologie révolutionnaire qui nous permet de donner vie à nos idées et de construire des pièces sur mesure pour nos robots. 
                        Grâce à des logiciels de modélisation avancés, nous concevons des pièces aux formes les plus complexes, puis les imprimons couche par couche, donnant naissance à des prototypes et des composants fonctionnels.<br />
                        <br />Mais l'impression 3D n'est qu'une partie de ce que nous faisons. Notre club explore également d'autres aspects de la robotique, comme la programmation, la mécatronique et l'intelligence artificielle. <br />
                        <br />Nous travaillons en équipe pour concevoir, construire et programmer des robots capables d'accomplir une variété de tâches, qu'il s'agisse de compétitions, de projets communautaires ou de défis techniques.<br />
                        <br />Lors de nos réunions hebdomadaires, nous échangeons des idées, partageons nos compétences et travaillons sur des projets passionnants. Nous invitons également des experts et des professionnels de l'industrie à intervenir, nous offrant ainsi l'opportunité d'apprendre de nouvelles techniques et de découvrir les dernières avancées en matière de robotique.
                    </p>
                </div>
                
            </section>

            <section class="sponsors"> <!--Possibilité de mettre un carrousel des sponsos ici quand il y en aura plus de 3. Sinon c'est un bandeau en bas de l'écran pour les partenariats-->
                <div class="sponso">              
                    <nav><a href="/partenariat/partenariat.php"><img src="img/partenariat.jfif" alt="partenariat1"></a></nav>
                </div>
                <div class="sponso">
                    <nav><a href="partenariat/partenariat.php"><img src="img/partenariat.jfif" alt="partenariat2"></a></nav>
                </div> 
                <div class="sponso">
                    <nav><a href="partenariat/partenariat.php"><img src="img/partenariat.jfif" alt="partenariat3"></a></nav>
                </div>
            </section>
            
        </main>

        <?php include('./header_footer_body/footer.php'); ?>  <!-- appel du fichier footer.php-->
    </body>
</html>
